#!/usr/bin/env python3

import argparse, random, pdb, math, os
import oyaml as yaml
import numpy as np

def seedPicker(seed1, seed2, team1=None, team2=None, total_seeds=16):
  # This function contains the secret sauce

  #Initialize Predictive Metrics
  coins1 = (total_seeds+1)-seed1
  coins2 = (total_seeds+1)-seed2

  heads1 = 0
  heads2 = 0

  #Simulate Game Statistics
  for ii in range(coins1):
    heads1 += random.random() > .5

  for ii in range(coins2):
    heads2 += random.random() > .5

  #Analyze Results
  if heads1 > heads2:
    winner = seed1
    winner_team = team1
  elif heads1 < heads2:
    winner = seed2
    winner_team = team2
  else: 
    # Tiebreaker: Go to overtime 
    if random.random() >.5:
      winner = seed1
      winner_team = team1
    else:
      winner = seed2
      winner_team = team2

  return winner, winner_team


class Bracket(object):
  """docstring for Bracket"""
  def __init__(self, q1_teams, q2_teams, q3_teams, q4_teams):
    # Dict of {seed: teamname}
    self.q1 = q1_teams
    self.q2 = q2_teams
    self.q3 = q3_teams
    self.q4 = q4_teams

    if not all([math.log2(len(x)).is_integer() for x in [self.q1, self.q2, self.q3, self.q4]]):
      raise ValueError("Number of teams needs to be 2^n")

    if not len(np.unique([len(x) for x in [self.q1, self.q2, self.q3, self.q4]])) == 1:
      raise ValueError("Number of teams needs to be the same")


  @classmethod
  def fromYaml(cls, yamlfile):
    with open(yamlfile, 'r') as f:
      teams_dict = yaml.load(f, Loader=yaml.FullLoader)

    midwest = teams_dict['midwest']
    south = teams_dict['south']
    east = teams_dict['east']
    west = teams_dict['west']

    return cls(west, east, south, midwest) 

  def solve(self):
    seeds = sorted(list(self.q1.keys()))

    q1_winner_seed, q1_winner = bracketAlgorithm(seeds, self.q1)
    q2_winner_seed, q2_winner = bracketAlgorithm(seeds, self.q2)
    q3_winner_seed, q3_winner = bracketAlgorithm(seeds, self.q3)
    q4_winner_seed, q4_winner = bracketAlgorithm(seeds, self.q4)

    print("\n====================")
    print("    SEMIFINALS")
    print("====================")
    sf1_winner_seed, sf1_winner = seedPicker(seed1=q1_winner_seed,
                                             seed2=q2_winner_seed,
                                             team1=q1_winner,
                                             team2=q2_winner,
                                             total_seeds=len(seeds)
                                            ) 

    sf2_winner_seed, sf2_winner = seedPicker(seed1=q3_winner_seed,
                                             seed2=q4_winner_seed,
                                             team1=q3_winner,
                                             team2=q4_winner,
                                             total_seeds=len(seeds)
                                            ) 

    printMatch(t1=q1_winner,
               t2=q2_winner,
               s1=q1_winner_seed,
               s2=q2_winner_seed,
               w=sf1_winner,
               ws=sf1_winner_seed)

    printMatch(t1=q3_winner,
               t2=q4_winner,
               s1=q3_winner_seed,
               s2=q4_winner_seed,
               w=sf2_winner,
               ws=sf2_winner_seed)


    print("\n====================")
    print("    GRAND FINALS")
    print("====================")
   
    winner_seed, winner = seedPicker(seed1=sf1_winner_seed,
                                     seed2=sf2_winner_seed,
                                     team1=sf1_winner,
                                     team2=sf2_winner,
                                     total_seeds=len(seeds)
                                    ) 

    printMatch(t1=sf1_winner,
               t2=sf2_winner,
               s1=sf1_winner_seed,
               s2=sf2_winner_seed,
               w=winner,
               ws=winner_seed)

def main():
    project_path = os.path.dirname(__file__) + "/.."
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-b', 
                        '--bracket',
                        type=str,
                        default='data/ncaa2021.yaml')

    args = parser.parse_args()

    b = Bracket.fromYaml(os.path.join(project_path, args.bracket))
    b.solve()


def bracketAlgorithm(seeds, teamnames):

  round_idx = 1
  while len(seeds) > 1:
    print(f"\nRound: {round_idx}")
    
    n_matches = int(len(seeds)/2)
    
    matches = [x for x in zip(seeds, seeds[::-1])][:n_matches]
    winners = [seedPicker(t1, t2, total_seeds=len(seeds))[0] for t1, t2 in matches]

    for match, winner in zip(matches, winners):
      printMatch(t1=teamnames[match[0]],
                 t2=teamnames[match[1]],
                 s1=match[0],
                 s2=match[1],
                 w=teamnames[winner],
                 ws=winner)

    seeds = winners
    round_idx += 1

  return seeds[0], teamnames[seeds[0]]

def printMatch(t1, t2, s1, s2, w, ws):
  print(f"\tMatch:  {t1} ({s1}) vs {t2} ({s2})")
  if ws > min(s1, s2):
    print(f"(!)\tWinner: {w} ({ws})")
  else:
    print(f"\tWinner: {w} ({ws})")


if __name__ == '__main__':
  main()